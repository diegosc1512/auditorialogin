﻿# Host: localhost:33065  (Version 5.5.5-10.1.30-MariaDB)
# Date: 2020-03-12 19:59:57
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "usuario"
#

CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `rol` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
