<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Usuario extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('usuario_model');
        $this->load->library('form_validation');
    }

    public function nuevo()
    {
        $this->load->view('crear_usuario');
    }
     public function datos($datos)
    {
        $data['data']=$datos;
        $this->load->view('vista_datos',$data);
    }
   
    public function index()
    {
        $data['msg'] = $this->uri->segment(3);
        $this->load->view('login_view', $data);
        
    }
   
    public function validarusr()
    {
        $nombre = $_POST['email'];
        $pass = md5($_POST['password']);
        $consulta = $this->usuario_model->validar($nombre, $pass);
        if ($consulta->num_rows() > 0) {
            foreach ($consulta->result() as $row) {  
                $this->session->set_userdata('nombre', $row->nombre);
                $this->session->set_userdata('rol', $row->rol);          
                $this->datos($consulta);
            }
        } 
        else
        {
            $data['msg'] = $this->uri->segment(3);
            $data['nota'] = "Error de credenciales";
            $this->load->view('login_view', $data);
        }
    }

    public function agregarbdusuario()
    {
        $data['nombre'] = $_POST['nombre'];
        $data['rol'] = $_POST['rol'];
        $data['pass'] = md5($_POST['password']);
        $this->usuario_model->agregarusuario($data);
        $this->index();
    }
}
