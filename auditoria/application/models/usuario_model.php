<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuario_model extends CI_Model {

	public function validar($email,$password)
	{
		$this->db->select('*');
		$this->db->from('usuario');
		$this->db->where('nombre',$email);
		$this->db->where('pass',$password);
		return $this->db->get();
	}
	
	public function agregarusuario($data)
	{
		$this->db->insert('usuario',$data);
	}
		public function agregarusuariorecuperarid($data)//para ajax
	{
		$this->db->insert('usuario',$data);
	 	return $this->db->insert_id();
	}
	
	public function modificarbdusuario($idusuario,$data)//para ajax
	{
		$this->db->where('idusuario',$idusuario);
		$this->db->update('usuario',$data);
	}
	public function eliminarusuario($idusuario,$data)//para ajax
	{
		$this->db->where('idusuario',$idusuario);
		$this->db->update('usuario',$data);
	}
	/**************/
	public function recuperarusuariopassword($idusuario)
	{
		$this->db->select("password");
 		$this->db->where("idusuario",$idusuario);
  		$resultado = $this->db->get("usuario");
  		return $resultado->row();
	}
}