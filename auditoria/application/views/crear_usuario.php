 <!DOCTYPE html>
<html lang="en" class="no-ie">
<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
   <meta name="description" content="">
   <meta name="keywords" content="">
   <meta name="author" content="">
   <title>Cuyotec</title>
   <link rel="stylesheet" href="../../app/css/bootstrap.css">
   <link rel="stylesheet" href="../../vendor/fontawesome/css/font-awesome.min.css">
   <link rel="stylesheet" href="../../vendor/animo/animate+animo.css">
   <link rel="stylesheet" href="../../vendor/csspinner/csspinner.min.css">
   <link rel="stylesheet" href="../../app/css/app.css">
   <script src="../../vendor/modernizr/modernizr.js" type="application/javascript"></script>
   <script src="../../vendor/fastclick/fastclick.js" type="application/javascript"></script>
   <script src="../../ajax/jquery.min.js"></script>
</head>

<body>
           <?php
echo form_open_multipart('usuario/agregarbdusuario');
?>
 <div class="panel-body">
                  <div class="form-group">
                    <input name="nombre" class="form-control" required type="text" placeholder="Nombre">
                  </div>
                  <div class="form-group">
                    <input name="password" class="form-control" minlength="6"  required type="Password" placeholder="Password">
                  </div>
                
                  <div class="form-group">
                  <select name="rol">
                    <option value="Desarollador">Desarollador</option>
                    <option value="Admin">Admin</option>
                     <option value="QA">QA</option>
                  </select>
                   </div>
                <div>
                  <button id="btnAgregar" class="btn btn-default" type="submit">Crear Usuario</button>
                </div>
              </div>
       
<?php
echo form_close();
?>

   <script src="../../../vendor/jquery/jquery.min.js"></script>
   <script src="../../../vendor/bootstrap/js/bootstrap.min.js"></script>
   <!-- Plugins-->
   <script src="../../../vendor/chosen/chosen.jquery.min.js"></script>
   <script src="../../../vendor/slider/js/bootstrap-slider.js"></script>
   <script src="../../../vendor/filestyle/bootstrap-filestyle.min.js"></script>
   <!-- Animo-->
   <script src="../../../vendor/animo/animo.min.js"></script>
   <!-- Sparklines-->
   <script src="../../../vendor/sparklines/jquery.sparkline.min.js"></script>
   <!-- Slimscroll-->
   <script src="../../../vendor/slimscroll/jquery.slimscroll.min.js"></script>
   <!-- START Page Custom Script-->
   <!-- END Page Custom Script-->
   <!-- App Main-->
   <script src="../../../app/js/app.js"></script>
   <!-- END Scripts-->
</body>

</html>